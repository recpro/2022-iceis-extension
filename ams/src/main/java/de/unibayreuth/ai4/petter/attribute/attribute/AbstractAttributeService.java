package de.unibayreuth.ai4.petter.attribute.attribute;

import de.unibayreuth.ai4.petter.attribute.model.Attribute;
import de.unibayreuth.ai4.petter.attribute.model.SenderResponse;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public abstract class AbstractAttributeService {
    public SenderResponse test() {
        return new SenderResponse("Test from attribute-backend");
    }
    abstract Set<Attribute> getAll();
    abstract Attribute getById(String attributeId);
    abstract Set<Attribute> getByActivityId(String activityId);

}
