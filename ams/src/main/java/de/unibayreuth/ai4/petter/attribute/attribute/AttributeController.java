package de.unibayreuth.ai4.petter.attribute.attribute;

import de.unibayreuth.ai4.petter.attribute.model.Attribute;
import de.unibayreuth.ai4.petter.attribute.model.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/attribute")

public class AttributeController {
    @Autowired
    AbstractAttributeService attributeService;

    @GetMapping(path = "/test")
    public SenderResponse test() {
        return attributeService.test();
    }

    @GetMapping(path = "/getAll")
    public ResponseEntity<Set<Attribute>> getAll() {
        return ResponseEntity.ok(attributeService.getAll());
    }

    @GetMapping(path = "/getById")
    public ResponseEntity<Attribute> getById(@RequestParam String attributeId) {
        return ResponseEntity.ok(attributeService.getById(attributeId));
    }
    @GetMapping(path = "/getByActivityId")
    public ResponseEntity<Set<Attribute>> getByActivityId(@RequestParam String activityId) {
        return ResponseEntity.ok(attributeService.getByActivityId(activityId));
    }

}
