package de.unibayreuth.ai4.petter.attribute.attribute;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.unibayreuth.ai4.petter.attribute.model.Attribute;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Profile("!EVALUATION")

public class JsonAttributeService extends AbstractAttributeService {
    @Override
    public Set<Attribute> getAll() {
        return this.getFromJson();
    }

    @Override
    public Attribute getById(String attributeId) {
        return this.getAll().stream().filter(c -> c.getId().equals(attributeId)).findFirst().orElse(new Attribute());
    }

    @Override
    Set<Attribute> getByActivityId(String activityId) {
        return this.getAll().stream().filter(c -> c.getActivityIds().stream().anyMatch(a -> a.equals(activityId))).collect(Collectors.toSet());
    }

    private Set<Attribute> getFromJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(new File("json/attributes.json"), new TypeReference<>(){});
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
        }
        return new HashSet<>();
    }

}
