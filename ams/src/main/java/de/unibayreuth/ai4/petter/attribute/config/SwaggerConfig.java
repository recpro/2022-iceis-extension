package de.unibayreuth.ai4.petter.attribute.config;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("attribute-public")
                .pathsToMatch("/api/**")
                .build();
    }
}
