package de.unibayreuth.ai4.petter.evaluation.api;

import de.unibayreuth.ai4.petter.evaluation.model.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "api/activity")
public class ActivityController {

    @Autowired
    EventLogService eventLogService;

    @GetMapping(path = "/getAll")
    public Set<Activity> getAll() {
        return eventLogService.getActivities();
    }
}
