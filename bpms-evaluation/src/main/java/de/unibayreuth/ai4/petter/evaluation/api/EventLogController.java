package de.unibayreuth.ai4.petter.evaluation.api;

import de.unibayreuth.ai4.petter.evaluation.model.EventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/eventlog")
public class EventLogController {

    @Autowired
    EventLogService eventLogService;

    @GetMapping(path = "/getAll")
    public EventLog getAll() {
        return eventLogService.getEventLog();
    }
}
