package de.unibayreuth.ai4.petter.evaluation.api;

import de.unibayreuth.ai4.petter.evaluation.loader.Loader;
import de.unibayreuth.ai4.petter.evaluation.model.*;
import de.unibayreuth.ai4.petter.evaluation.parser.Parser;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XOrganizationalExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Service
public class EventLogService {

    @Autowired
    Parser parser;

    XLog log;

    String processLogPath = "data/Helpdesk.xes";

    public EventLogService() {
        Loader loader = Loader.getInstance();
        this.log = loader.getProcessLog(processLogPath);
    }

    public Set<Activity> getActivities() {
        Set<Activity> activities = new HashSet<>();
        Set<String> acts = parser.getActivities(log);
        acts.forEach(a -> {
            if (validActivity(a)) {
                activities.add(new Activity(a.trim().toLowerCase(), a));
            }
        });
        activities.add(new Activity("inform ticket applicant", "inform ticket applicant"));
        return activities;
    }

    public EventLog getEventLog() {
        EventLog eventLog = new EventLog();
        for (XTrace currentTrace : log) {
            for (XEvent currentEvent : currentTrace) {
                XAttributeMap attributes = currentEvent.getAttributes();
                String activity = attributes.get(XConceptExtension.KEY_NAME).toString().trim();
                if (validActivity(activity)) {
                    attributes.put("rating", new XAttributeLiteralImpl("rating", "1"));
                    currentEvent.setAttributes(attributes);

                    String assigneeId = attributes.get(XOrganizationalExtension.KEY_RESOURCE).toString();
                    int rating = (int) Math.round(Math.random());
                    if (assigneeId.equals("Value 1")) {
                        assigneeId = "337afdf6-69e1-4c37-9a99-bd1c429bd41e";
//                        if (activity.equals("Assign seriousness")) {
//                            continue;
//                        }
                    }
                    Task task = new Task(
                            "0",
                            new Activity(activity.toLowerCase(), activity),
                            new HashSet<>(),
                            "{}",
                            50,
                            assigneeId,
                            "description",
                            new ProcessInstance(),
                            Instant.parse(attributes.get(XTimeExtension.KEY_TIMESTAMP).toString())

                    );
                    Event event = new Event(
                            "0",
                            task,
                            attributes.get(XLifecycleExtension.KEY_TRANSITION).toString(),
                            Instant.parse(attributes.get(XTimeExtension.KEY_TIMESTAMP).toString()),
                            Instant.parse(attributes.get(XTimeExtension.KEY_TIMESTAMP).toString()),
                            new Rating("0", rating)
                    );
                    eventLog.getEvents().add(event);
                }
            }
        }
        return eventLog;
    }

    public boolean validActivity(String a) {
        return !(a.equals("Closed") || a.equals("DUPLICATE") || a.equals("INVALID") || a.equals("RESOLVED") || a.equals("VERIFIED") || a.equals("Wait"));
    }

}
