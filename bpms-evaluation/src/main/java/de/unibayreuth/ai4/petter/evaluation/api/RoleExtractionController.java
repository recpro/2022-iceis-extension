package de.unibayreuth.ai4.petter.evaluation.api;

import de.unibayreuth.ai4.petter.evaluation.loader.Loader;
import de.unibayreuth.ai4.petter.evaluation.modification.ModificationService;
import de.unibayreuth.ai4.petter.evaluation.parser.Parser;
import de.unibayreuth.ai4.petter.evaluation.parser.RecProParser;
import de.unibayreuth.ai4.petter.evaluation.parser.organizational.RoleExtractor;
import org.deckfour.xes.model.XLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(path = "/api/roleExtraction")
public class RoleExtractionController {

    @Autowired
    ModificationService modificationService;

    @Autowired
    RecProParser recProParser;

    @GetMapping("/test")
    public void test() {
        RoleExtractor roleExtractor = new RoleExtractor();
        Loader loader = Loader.getInstance();
        XLog log = loader.getProcessLog("data/Helpdesk.xes");

        Map<String, Integer[]> map_without_lifecyle = roleExtractor.extractProfiles(log, false);
        Map<String, Integer[]> map_with_lifecyle = roleExtractor.extractProfiles(log, true);
        for(String s : map_without_lifecyle.keySet()) {
            String temp = s+"\t";
            Integer[] currentProfile = map_without_lifecyle.get(s);
            temp = temp+"[";
            for(int i=0; i < currentProfile.length; i++) {
                temp = temp+currentProfile[i]+",";
            }
            temp = temp+"]";
            System.out.println(temp);
        }

        Parser p = new Parser();
        System.out.println(p.getOriginators(log).size());
        Map<String, Set<String>> roles = roleExtractor.extractRoles(map_without_lifecyle, 0.85);
        for(String role : roles.keySet()) {
            System.out.println(role);
            System.out.println("\t"+roles.get(role));
        }

        for(String s : map_with_lifecyle.keySet()) {
            String temp = s+"\t";
            Integer[] currentProfile = map_with_lifecyle.get(s);
            temp = temp+"[";
            for(int i=0; i < currentProfile.length; i++) {
                temp = temp+currentProfile[i]+",";
            }
            temp = temp+"]";

        }

        System.out.println(p.getOriginators(log).size());
        Map<String, Set<String>> rolesWithLifecyle = roleExtractor.extractRoles(map_with_lifecyle, 0.85);
        for(String role : rolesWithLifecyle.keySet()) {
            System.out.println(role);
            System.out.println("\t"+rolesWithLifecyle.get(role));
        }

        Set<String> activities = p.getActivities(log);
        activities.forEach(System.out::println);

        Set<Map<String, String>> activitiesAndUsers = p.getActivitiesAndUser(log, "Value 4");
        activitiesAndUsers.forEach(System.out::println);

        modificationService.addRatings(log);
//        return recProParser.getFull(log);

    }
}
