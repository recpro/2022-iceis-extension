package de.unibayreuth.ai4.petter.evaluation.config;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("evaluation-public")
                .pathsToMatch("/api/**")
                .build();
    }
}
