package de.unibayreuth.ai4.petter.evaluation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Event {
    private String id;
    private Task task;
    private String status;
    private Instant startTime;
    private Instant endTime;
    private Rating rating;
}
