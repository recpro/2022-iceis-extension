package de.unibayreuth.ai4.petter.evaluation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProcessInstance {

    private String id = "";
    private String description = "";
    private Process process = new Process();
}
