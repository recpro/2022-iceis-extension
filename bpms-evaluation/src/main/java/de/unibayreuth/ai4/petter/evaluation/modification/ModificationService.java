package de.unibayreuth.ai4.petter.evaluation.modification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.extension.XExtensionParser;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XOrganizationalExtension;
import org.deckfour.xes.model.*;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XLogImpl;
import org.springframework.stereotype.Service;

import java.util.Iterator;

@Service
@NoArgsConstructor
@Getter
@Setter
public class ModificationService {

    public XLog addRatings(XLog log) {
        Iterator<XTrace> logIterator = log.iterator();
        while(logIterator.hasNext()) {
            XTrace currentTrace = logIterator.next();
            Iterator<XEvent> traceIterator = currentTrace.iterator();
            while(traceIterator.hasNext()) {
                XEvent currentEvent = traceIterator.next();
                XAttributeMap attributes = currentEvent.getAttributes();
                attributes.put("rating", new XAttributeLiteralImpl("rating", "1"));
                currentEvent.setAttributes(attributes);
            }
        }
        return log;
    }

}
