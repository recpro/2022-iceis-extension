package de.unibayreuth.ai4.petter.evaluation.parser;

import de.unibayreuth.ai4.petter.evaluation.parser.classifier.Classifier;

import java.util.ArrayList;
import java.util.List;

public class TraceVariant {
    List<Classifier> events = new ArrayList<Classifier>();

    public TraceVariant() {

    }

    public void addEvent(Classifier classifier) {
        events.add(classifier);
    }

    public List<Classifier> getEvents() {
        return events;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof TraceVariant)) {
            return false;
        }
        else {
            TraceVariant c = (TraceVariant) o;
            return (events.equals(c.getEvents()));
        }
    }

    @Override
    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < events.size(); i++) {
            sb.append(events.get(i));
            if(i != events.size()-1) {
                sb.append("\t-->");
            }
        }
        return sb.toString();
    }

}
