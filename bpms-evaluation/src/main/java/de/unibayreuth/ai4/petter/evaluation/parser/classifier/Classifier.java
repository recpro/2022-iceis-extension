package de.unibayreuth.ai4.petter.evaluation.parser.classifier;

public abstract class Classifier {
    public abstract boolean equals(Object o);
    public abstract int hashCode();
}
