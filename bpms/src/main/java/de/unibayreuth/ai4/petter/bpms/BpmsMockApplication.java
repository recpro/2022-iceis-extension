package de.unibayreuth.ai4.petter.bpms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BpmsMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpmsMockApplication.class, args);
	}

}
