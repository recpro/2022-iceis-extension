package de.unibayreuth.ai4.petter.bpms.activity;

import de.unibayreuth.ai4.petter.bpms.model.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/bpms/activity")
public class ActivityController {

    @Autowired
    ActivityService activityService;

    @GetMapping(path = "/getAll")
    public ResponseEntity<Set<Activity>> getAll() {
        return ResponseEntity.ok(activityService.getAll());
    }

    @GetMapping(path = "/getById")
    public ResponseEntity<Activity> getById(@RequestParam(name = "activityId") String activityId) {
        return ResponseEntity.ok(activityService.getById(activityId));
    }
}
