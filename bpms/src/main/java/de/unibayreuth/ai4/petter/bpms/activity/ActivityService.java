package de.unibayreuth.ai4.petter.bpms.activity;

import de.unibayreuth.ai4.petter.bpms.model.Activity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
public abstract class ActivityService {

    public Set<Activity> getAll() {
        return new HashSet<>();
    }

    public Activity getById(String activityId) {
        Set<Activity> activities = this.getAll();
        return !activities.isEmpty() ? activities.stream().filter(a -> Objects.equals(a.getId(), activityId)).toList().stream().findFirst().orElse(new Activity()) : new Activity();
    }
}
