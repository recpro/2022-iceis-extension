package de.unibayreuth.ai4.petter.bpms.activity;

import de.unibayreuth.ai4.petter.bpms.model.Activity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@Profile("EVALUATION")
@Service
public class EvaluationActivityService extends ActivityService {

    @Value("${recpro.evaluation.url}")
    private String url;

    @Override
    public Set<Activity> getAll() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Set<Activity>> activities = restTemplate.exchange(url + "/activity/getAll",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });
        return activities.getBody();
    }

}
