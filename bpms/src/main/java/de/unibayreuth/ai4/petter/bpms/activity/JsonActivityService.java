package de.unibayreuth.ai4.petter.bpms.activity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.unibayreuth.ai4.petter.bpms.model.Activity;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

@Profile("!EVALUATION")
@Service
public class JsonActivityService extends ActivityService {

    @Override
    public Set<Activity> getAll() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(new File("json/activities.json"), new TypeReference<>(){});
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
        }
        return new HashSet<>();
    }
}
