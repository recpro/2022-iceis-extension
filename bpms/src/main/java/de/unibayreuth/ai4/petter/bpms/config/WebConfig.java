package de.unibayreuth.ai4.petter.bpms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Profile("dev")
public class WebConfig {
    private final long MAX_AGE_SECS = 3600;

    @Value("${recpro.bpms.baseurl.dev}")
    String[] baseUrl;

    @Bean
    public WebMvcConfigurer corsDevConfigurer() {
        final String[] finalBaseUrl = this.baseUrl;
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins(finalBaseUrl)
                        .allowedMethods("PUT", "DELETE", "GET", "POST", "OPTIONS").maxAge(MAX_AGE_SECS);
            }

        };
    }
}
