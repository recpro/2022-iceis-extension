package de.unibayreuth.ai4.petter.bpms.eventlog;


import de.unibayreuth.ai4.petter.bpms.model.EventLog;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Profile("EVALUATION")
@Service
public class EvaluationEventLogService extends EventLogService {

    @Value("${recpro.evaluation.url}")
    private String url;

    @Override
    public EventLog getAll() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/eventlog/getAll", EventLog.class);
    }
}
