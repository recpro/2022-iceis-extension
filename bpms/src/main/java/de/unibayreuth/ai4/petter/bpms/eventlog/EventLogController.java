package de.unibayreuth.ai4.petter.bpms.eventlog;

import de.unibayreuth.ai4.petter.bpms.model.EventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/bpms/eventlog")
public class EventLogController {

    @Autowired
    EventLogService eventLogService;

    @GetMapping(path="/getAll")
    public ResponseEntity<EventLog> getAll() {
        return ResponseEntity.ok(eventLogService.getAll());
    }

    @GetMapping(path="/getByUser")
    public ResponseEntity<EventLog> getByUser(@RequestParam(name="assigneeId") String assigneeId) {
        return ResponseEntity.ok(eventLogService.getByUser(assigneeId));
    }
}
