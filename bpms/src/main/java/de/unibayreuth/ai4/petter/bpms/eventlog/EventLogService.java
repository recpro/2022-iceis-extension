package de.unibayreuth.ai4.petter.bpms.eventlog;

import de.unibayreuth.ai4.petter.bpms.model.EventLog;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public abstract class EventLogService {

    public EventLog getAll() {
        return new EventLog();
    }

    public EventLog getByUser(String assigneeId) {
        return new EventLog(this.getAll().getEvents().stream().filter(e -> e.testOnSameUser(assigneeId)).collect(Collectors.toSet()));
    }
}
