package de.unibayreuth.ai4.petter.bpms.eventlog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.unibayreuth.ai4.petter.bpms.model.EventLog;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;

@Profile("!EVALUATION")
@Service
public class JsonEventLogService extends EventLogService {

    @Override
    public EventLog getAll() {
        ObjectMapper objectMapper = new ObjectMapper();
        JavaTimeModule module = new JavaTimeModule();
        objectMapper.registerModule(module);
        try {
            return objectMapper.readValue(new File("json/eventlog.json"), EventLog.class);
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
        }
        return null;
    }
}
