package de.unibayreuth.ai4.petter.bpms.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Event {
    private String id = "";
    private Task task = new Task();
    private String status = "";
    private Instant startTime = Instant.now();
    private Instant endTime = Instant.now();
    private Rating rating = new Rating();

    public boolean testOnSameUser(String assigneeId) {
        return this.getTask().getAssigneeId().equals(assigneeId);
    }

}
