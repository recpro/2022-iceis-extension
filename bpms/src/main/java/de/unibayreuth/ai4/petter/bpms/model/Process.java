package de.unibayreuth.ai4.petter.bpms.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
public class Process {
    private String id;
    private String name;
    private String description;
    private Set<Activity> activities;

    public Process() {
        this.id = "";
        this.name = "";
        this.description = "";
        this.activities = new HashSet<>();
    }
}
