package de.unibayreuth.ai4.petter.bpms.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Task {

    private String id = "";
    private Activity activity = new Activity();
    private String data = "";
    private int priority = 0;
    private String assigneeId = "";
    private String description = "";
    private ProcessInstance processInstance = new ProcessInstance();
    private Instant createdAt = Instant.now();
}
