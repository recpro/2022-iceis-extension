package de.unibayreuth.ai4.petter.bpms.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Worklist {
    private String id = "";
    private Set<Task> tasks = new HashSet<>();
}
