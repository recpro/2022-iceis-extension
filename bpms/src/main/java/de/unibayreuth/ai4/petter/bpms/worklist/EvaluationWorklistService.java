package de.unibayreuth.ai4.petter.bpms.worklist;

import de.unibayreuth.ai4.petter.bpms.model.Activity;
import de.unibayreuth.ai4.petter.bpms.model.ProcessInstance;
import de.unibayreuth.ai4.petter.bpms.model.Task;
import de.unibayreuth.ai4.petter.bpms.model.Worklist;
import org.hibernate.jdbc.Work;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashSet;

@Profile("EVALUATION")
@Service
public class EvaluationWorklistService extends WorklistService {
    @Override
    public Worklist getWorklist() {
        Worklist result = new Worklist();
        result.setId("1");
        result.getTasks().add(
                new Task(
                        "0",
                        new Activity("assign seriousness", "Assign seriousness"),
                        "{}",
                        50,
                        "",
                        "description",
                        new ProcessInstance(),
                        Instant.now()
                )
        );
        return result;
    }
}
