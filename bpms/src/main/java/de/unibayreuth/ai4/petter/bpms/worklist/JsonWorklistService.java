package de.unibayreuth.ai4.petter.bpms.worklist;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.unibayreuth.ai4.petter.bpms.model.Worklist;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;

@Profile("!EVALUATION")
@Service
public class JsonWorklistService extends WorklistService {

    @Override
    public Worklist getWorklist() {
        return this.readWorklistFromJson();
    }

    private Worklist readWorklistFromJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        JavaTimeModule module = new JavaTimeModule();
        objectMapper.registerModule(module);
        try {
            return objectMapper.readValue(new File("json/worklist.json"), Worklist.class);
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
        }
        return null;
    }
}
