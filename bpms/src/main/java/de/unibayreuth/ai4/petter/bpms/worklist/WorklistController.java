package de.unibayreuth.ai4.petter.bpms.worklist;

import de.unibayreuth.ai4.petter.bpms.model.SenderResponse;
import de.unibayreuth.ai4.petter.bpms.model.Worklist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/bpms/worklist")
public class WorklistController {

    @Autowired
    WorklistService worklistService;

    @GetMapping(path = "test")
    public SenderResponse test() {
        return worklistService.test();
    }
    @GetMapping(path="/getWorklist")
    public ResponseEntity<Worklist> getWorklist() {
        return ResponseEntity.ok(worklistService.getWorklist());
    }
}
