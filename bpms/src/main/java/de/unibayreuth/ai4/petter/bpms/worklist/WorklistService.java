package de.unibayreuth.ai4.petter.bpms.worklist;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.unibayreuth.ai4.petter.bpms.model.SenderResponse;
import de.unibayreuth.ai4.petter.bpms.model.Worklist;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public abstract class WorklistService {

    public Worklist getWorklist() {
        return new Worklist();
    }

    public SenderResponse test() {
        return new SenderResponse("Test from BPMS-backend");
    }



}
