package de.unibayreuth.ai4.petter.backend;

import de.unibayreuth.ai4.petter.backend.attributes.AttributeService;
import de.unibayreuth.ai4.petter.backend.model.Rating;
import de.unibayreuth.ai4.petter.backend.model.User;
import de.unibayreuth.ai4.petter.backend.model.bpmn.*;
import de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation.Item;
import de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation.ItemVector;
import de.unibayreuth.ai4.petter.backend.model.recpro.*;
import de.unibayreuth.ai4.petter.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ObjectCreationHelper {

    @Autowired
    AttributeService attributesService;

    @Autowired
    UserService userService;

    public RecproActivity getRecproActivity(Activity activity) {
        RecproActivity result = new RecproActivity();
        result.setId(activity.getId());
        result.setName(activity.getName());
        return result;
    }

    public RecproEvent getRecproEvent(Event event) {
        RecproEvent result = new RecproEvent();
        result.setId(event.getId());
        result.setTask(this.getRecproTask(event.getTask()));
        result.setStatus(event.getStatus());
        result.setStartTime(event.getStartTime());
        result.setEndTime(event.getEndTime());
        result.setRating(this.getRecproRating(event.getRating()));

        return result;
    }

    public RecproEventLog getRecproEventLog(EventLog eventLog) {
        RecproEventLog result = new RecproEventLog();
        eventLog.getEvents().forEach(e -> {
            result.getEvents().add(this.getRecproEvent(e));
        });
        return result;
    }

//    public RecproProcessInstance getRecproProcessInstance(ProcessInstance processInstance) {
//        RecproProcessInstance result = new RecproProcessInstance();
//        result.setId(processInstance.getId());
//        result.setDescription(processInstance.getDescription());
//        result.setProcess(processService.getById(processInstance.getProcessId()));
//        return result;
//    }

    public RecproRating getRecproRating(Rating rating) {
        RecproRating result = new RecproRating();
        result.setId(rating.getId());
        result.setRating(rating.getRating());
        return result;
    }

    public RecproTask getRecproTask(Task task) {
        RecproTask result = new RecproTask();
        result.setId(task.getId());
        result.setActivity(this.getRecproActivity(task.getActivity()));
        result.setData(task.getData());
        result.setPriority(task.getPriority());
        result.setAssignee(userService.getById(task.getAssigneeId()));
        result.setCreatedAt(task.getCreatedAt());
        result.setDescription(task.getDescription());
        return result;
    }

    public RecproUser getRecproUser(User user) {
        RecproUser result = new RecproUser();
        result.setId(user.getId());
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setAttributes(this.getAttributesById(user.getAttributeIds()));
        return result;
    }

    public RecproWorklist getRecproWorklist(Worklist worklist, Set<RecproTask> tasks) {
        RecproWorklist result = new RecproWorklist();
        result.setId(worklist.getId());
        result.setTasks(tasks);
        return result;
    }

    private Set<RecproAttribute> getAttributesById(Set<String> attributeIds) {
        Set<RecproAttribute> attributesHelper = new HashSet<>();
        attributeIds.forEach(cid -> attributesHelper.add(attributesService.getById(cid)));
        return attributesHelper;
    }

    public Item getItem(Activity activity) {
        Item result = new Item();
        result.setId(activity.getId());
        attributesService.getAll().stream().forEach(c -> {
            result.getVector().add(new ItemVector(c.getId(), c.getActivityIds().contains(activity.getId()) ? 1.0 : 0));
        });
        return result;
    }
}
