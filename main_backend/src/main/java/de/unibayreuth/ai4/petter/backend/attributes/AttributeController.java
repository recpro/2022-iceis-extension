package de.unibayreuth.ai4.petter.backend.attributes;

import de.unibayreuth.ai4.petter.backend.model.recpro.RecproAttribute;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/attribute")
public class AttributeController {

    @Autowired
    AttributeService attributesService;

    @GetMapping(path = "/test")
    public SenderResponse test() {
        return attributesService.test();
    }

    @GetMapping(path = "/getAll")
    public ResponseEntity<Set<RecproAttribute>> getAll() {
        return ResponseEntity.ok(attributesService.getAll());
    }

    @GetMapping(path = "/getById")
    public ResponseEntity<RecproAttribute> getAll(@RequestParam String attributeId) {
        return ResponseEntity.ok(attributesService.getById(attributeId));
    }

    @GetMapping(path = "/getByActivityId")
    public ResponseEntity<Set<RecproAttribute>> getByActivityId(@RequestParam String activityId) {
        return ResponseEntity.ok(attributesService.getByActivityId(activityId));
    }
}
