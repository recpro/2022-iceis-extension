package de.unibayreuth.ai4.petter.backend.attributes;

import de.unibayreuth.ai4.petter.backend.model.recpro.RecproAttribute;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@Service
@PropertySource("classpath:application.properties")
public class AttributeService {

    @Value("${recpro.attribute.url}")
    private String url;

    public SenderResponse test() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/attribute/test", SenderResponse.class);
    }

    public Set<RecproAttribute> getAll() {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Set<RecproAttribute>> attribute = restTemplate.exchange(
                url + "/attribute/getAll",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });
        return attribute.getBody();
    }

    public RecproAttribute getById(String id) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<RecproAttribute> attribute = restTemplate.exchange(
                url + "/attribute/getById?attributeId=" + id,
                HttpMethod.GET,
                null,
                RecproAttribute.class);
        return attribute.getBody();
    }

    public Set<RecproAttribute> getByActivityId(String activityId) {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Set<RecproAttribute>> attributes = restTemplate.exchange(
                url + "/attribute/getByActivityId?activityId=" + activityId,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });
        return attributes.getBody();
    }
}
