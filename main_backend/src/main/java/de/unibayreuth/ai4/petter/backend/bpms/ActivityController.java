package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.model.bpmn.Activity;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/bpms/activity")
public class ActivityController {

    @Autowired
    ActivityService activityService;

    @GetMapping(path = "/getAll")
    public ResponseEntity<Set<Activity>> getAll() {
        return ResponseEntity.ok(activityService.getAll());
    }

    @GetMapping(path = "/getById")
    public ResponseEntity<Activity> getById(@RequestParam(name = "activityId") String activityId) {
        return ResponseEntity.ok(activityService.getById(activityId));
    }
}
