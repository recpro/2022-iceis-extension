package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.attributes.AttributeService;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Activity;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@Service
@PropertySource("classpath:application.properties")
public class ActivityService {

    @Value("${recpro.bpms.url}")
    private String url;

    @Autowired
    AttributeService attributeService;

    @Autowired
    ObjectCreationHelper objectCreationHelper;

    public SenderResponse test() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/bpms/activity/test", SenderResponse.class);
    }

    public Set<Activity> getAll() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Set<Activity>> result = restTemplate.exchange(
                url + "/activity/getAll",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });
        return result.getBody();
    }

    public Activity getById(String activityId) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Activity> result = restTemplate.exchange(url + "/activity/getById?activityId=" + activityId,
                HttpMethod.GET,
                null,
                Activity.class);
        return result.getBody();
    }
}
