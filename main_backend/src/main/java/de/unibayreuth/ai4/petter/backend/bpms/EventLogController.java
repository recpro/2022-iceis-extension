package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.model.bpmn.EventLog;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproEventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/bpms/eventlog")
public class EventLogController {

    @Autowired
    EventLogService eventLogService;

    @GetMapping(path = "/getEventLog")
    public ResponseEntity<EventLog> geEventLog() {
        return ResponseEntity.ok(eventLogService.getEventLog());
    }

    @GetMapping(path = "/getEventLogByUser")
    public ResponseEntity<EventLog> geEventLogByUser(@RequestParam(name = "assigneeId") String assigneeId) {
        return ResponseEntity.ok(eventLogService.getEventLogByUser(assigneeId));
    }
}
