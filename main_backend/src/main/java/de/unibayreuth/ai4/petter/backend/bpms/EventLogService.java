package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Event;
import de.unibayreuth.ai4.petter.backend.model.bpmn.EventLog;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproActivity;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproEvent;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproEventLog;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproRating;
import de.unibayreuth.ai4.petter.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EventLogService {
    @Value("${recpro.bpms.url}")
    private String url;

    public EventLog getEventLog() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/eventlog/getAll", EventLog.class);
    }

    public EventLog getEventLogByUser(String assigneeId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/eventlog/getByUser?assigneeId=" + assigneeId, EventLog.class);
    }

    public Double getAverageRating(Set<Event> events) {
        return (double) (events.stream().map(e -> e.getRating().getRating()).reduce(0, Integer::sum)) / events.size();
    }

    public Set<Event> getEventsByActivityAndAssignee(EventLog eventLog, String activityId, String assigneeId) {
        Set<Event> events = eventLog.getEvents().stream().filter(event -> event.getTask().getActivity().getId().equals(activityId) && event.getTask().getAssigneeId().equals(assigneeId)).collect(Collectors.toSet());
        return events;
    }
}
