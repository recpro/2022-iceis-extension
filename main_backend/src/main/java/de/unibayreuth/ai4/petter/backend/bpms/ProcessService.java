package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Process;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproEvent;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ProcessService {

    @Autowired
    ObjectCreationHelper objectCreationHelper;

    @Autowired
    ActivityService activityService;

    public Set<RecproProcess> getAll() {
        Set<RecproProcess> result = new HashSet<>();
        return result;
    }

    private Set<Process> getAllFromSource() {
        return new HashSet<>();
    }

    public RecproProcess getById(String id) {

        return new RecproProcess();
    }

    private Process getByIdFromSource(String id) {
        return new Process();
    }
}
