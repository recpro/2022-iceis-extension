package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.model.bpmn.Worklist;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproWorklist;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/bpms/worklist")
public class WorklistController {

    @Autowired
    WorklistService worklistService;

    @GetMapping(path="test")
    public SenderResponse test() {
        return worklistService.test();
    }

    @GetMapping(path = "/getWorklist")
    public ResponseEntity<Worklist> getWorklist() {
        return ResponseEntity.ok(worklistService.getWorklist());
    }
}
