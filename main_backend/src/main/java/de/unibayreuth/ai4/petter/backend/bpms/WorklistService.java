package de.unibayreuth.ai4.petter.backend.bpms;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Activity;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Task;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Worklist;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproTask;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproWorklist;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import de.unibayreuth.ai4.petter.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

@Service
@PropertySource("classpath:application.properties")
public class WorklistService {

    @Value("${recpro.bpms.url}")
    private String url;

    public SenderResponse test() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/worklist/test", SenderResponse.class);
    }


    public Worklist getWorklist() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/worklist/getWorklist", Worklist.class);
    }
}
