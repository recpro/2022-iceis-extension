package de.unibayreuth.ai4.petter.backend.calculation.contentBased;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.bpms.ActivityService;
import de.unibayreuth.ai4.petter.backend.bpms.WorklistService;
import de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation.*;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproWorklist;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/api/calculation")
public class ContentBasedController {

    @Autowired
    ContentBasedService contentBasedService;

    @Autowired
    ActivityService activityService;

    @Autowired
    ObjectCreationHelper objectCreationHelper;

    @Autowired
    WorklistService worklistService;

    @GetMapping(path = "/test")
    public SenderResponse test() {
        return contentBasedService.test();
    }

    @GetMapping(path = "/getDistance")
    public ResponseEntity<Distance> getDistance() {
        ItemSet itemSet = new ItemSet();
        itemSet.setA(objectCreationHelper.getItem(activityService.getById("4")));
        HashSet<Item> items = new HashSet<>();
        items.add(objectCreationHelper.getItem(activityService.getById("5")));
        itemSet.setItems(items);
        return ResponseEntity.ok(this.contentBasedService.getDistance(itemSet));
    }

    @GetMapping(path = "/minDistance")
    public ResponseEntity<Distance> getMinDistance() {
        ItemSet itemSet = new ItemSet();
        itemSet.setA(objectCreationHelper.getItem(activityService.getById("4")));
        HashSet<Item> items = new HashSet<>();
        items.add(objectCreationHelper.getItem(activityService.getById("5")));
        items.add(objectCreationHelper.getItem(activityService.getById("1")));
        items.add(objectCreationHelper.getItem(activityService.getById("2")));
        items.add(objectCreationHelper.getItem(activityService.getById("3")));
        itemSet.setItems(items);
        return ResponseEntity.ok(this.contentBasedService.getMinDistance(itemSet));
    }

    @GetMapping(path = "/allDistances")
    public ResponseEntity<Set<DistanceCalculation>> getAllDistances() {
        return ResponseEntity.ok(this.contentBasedService.getAllDistanceCalculations());
    }

        @GetMapping(path = "/calculate")
    public ResponseEntity<Set<ContentBasedResult>> calculate(@RequestParam String assigneeId) {
        Set<ContentBasedResult> result = this.contentBasedService.calculateResultForWorklist(this.worklistService.getWorklist(), assigneeId);
        return ResponseEntity.ok(result);
    }

//    @GetMapping(path = "/allDistances")
//    public ResponseEntity<List<DistanceCalculation>> allDistances() {
//        return this.contentBasedService.distances();
//    }
//
//
//    @GetMapping(path = "/average")
//    public void calculateAverage(@RequestParam String assigneeId) {
//        this.contentBasedService.calculateAverage(assigneeId);
//    }
//
//    @GetMapping(path = "/calculate")
//    public ResponseEntity<Set<ContentBasedCalculationResult>> calculate(@RequestParam String assigneeId) {
//        System.out.println(assigneeId);
//        return this.contentBasedService.calculate(assigneeId);
//    }
}
