package de.unibayreuth.ai4.petter.backend.calculation.contentBased;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.bpms.ActivityService;
import de.unibayreuth.ai4.petter.backend.bpms.EventLogService;
import de.unibayreuth.ai4.petter.backend.bpms.WorklistService;
import de.unibayreuth.ai4.petter.backend.attributes.AttributeService;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Activity;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Event;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Worklist;
import de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation.*;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class ContentBasedService {

    @Autowired
    ActivityService activityService;
    @Autowired
    AttributeService attributeService;

    @Autowired
    EventLogService eventLogService;

    @Autowired
    WorklistService worklistService;

    @Autowired
    ObjectCreationHelper objectCreationHelper;

    @Value("${recpro.contentbasedcalculation.url}")
    private String url;

    public SenderResponse test() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/calculation/test", SenderResponse.class);
    }

    public Distance getDistance(ItemSet items) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ItemSet> request = new HttpEntity<>(
                items,
                headers);

        ResponseEntity<Distance> result = restTemplate.exchange(
                url + "/contentBased/distance",
                HttpMethod.POST,
                request,
                Distance.class);
        return result.getBody();
    }

    public Distance getMinDistance(ItemSet items) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ItemSet> request = new HttpEntity<>(
                items,
                headers);

        ResponseEntity<Distance> result = restTemplate.exchange(
                url + "/contentBased/minDistance",
                HttpMethod.POST,
                request,
                Distance.class);
        return result.getBody();
    }

    public Set<Distance> getAllDistancesForItem(ItemSet items) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ItemSet> request = new HttpEntity<>(
                items,
                headers);

        ResponseEntity<Set<Distance>> result = restTemplate.exchange(
                url + "/contentBased/allDistancesForItem",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<>() {});
        return result.getBody();
    }

    public Set<DistanceCalculation> getAllDistanceCalculations() {
        Set<DistanceCalculation> distances = new HashSet<>();
        this.getAllDistances().forEach(d -> distances.add(
                new DistanceCalculation(
                        activityService.getById(d.getA().getId()),
                        activityService.getById(d.getB().getId()),
                        d.getDistance()
                )));
        return distances;
    }

    public Set<Distance> getAllDistances() {
        Set<Activity> activities = activityService.getAll();
        HashSet<Item> items = new HashSet<>();
        activities.forEach(a -> items.add(objectCreationHelper.getItem(a)));
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ItemSet> request = new HttpEntity<>(
                new ItemSet(new Item(), items),
                headers);

        ResponseEntity<Set<Distance>> result = restTemplate.exchange(
                url + "/contentBased/allDistances",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<>() {});
        return result.getBody();
    }

    public Set<ContentBasedResult> calculateResultForWorklist(Worklist worklist, String assigneeId) {
        Set<ContentBasedResult> result = new HashSet<>();
        worklist.getTasks().forEach(t -> result.add(this.calculateResult(t.getActivity(), assigneeId)));
        return result;
    }

    public ContentBasedResult calculateResult(Activity activity, String assigneeId) {
        Set<Event> events = eventLogService.getEventsByActivityAndAssignee(eventLogService.getEventLog(), activity.getId(), assigneeId);
        Set<ContentBasedResult> result = new HashSet<>();
        if (events.isEmpty()) {
            Set<Activity> activities = activityService.getAll();
            ItemSet items = new ItemSet();
            items.setA(objectCreationHelper.getItem(activity));
            activities.forEach(b -> items.getItems().add(objectCreationHelper.getItem(b)));
            List<Distance> distances = new ArrayList<>(this.getAllDistancesForItem(items));
            distances.sort(Comparator.comparingDouble(Distance::getDistance).reversed());

            Distance distance = distances.stream().filter(d -> !eventLogService.getEventsByActivityAndAssignee(eventLogService.getEventLog(), d.getB().getId(), assigneeId).isEmpty()).findFirst().orElse(new Distance());

            if (distance.getDistance() != -1) {
                return this.getBasicResult(activity.getId(), distance.getB().getId(), eventLogService.getEventsByActivityAndAssignee(eventLogService.getEventLog(), distance.getB().getId(), assigneeId), distance.getDistance());
            }
        }
        return this.getBasicResult(activity.getId(), activity.getId(), events, 1.0);
    }

    private ContentBasedResult getBasicResult(String a, String b, Set<Event> events, Double distance) {
        ContentBasedResult result = new ContentBasedResult();
        result.setA(a);
        if (a.equals(b)) {
            result.setRatingCount(events.size());
        } else {
            result.setRatingCount(0);
        }
        result.setSimilarity(distance);
        result.setB(b);
        if (events.isEmpty()) {
            result.setPredictedRating(-1);
        } else {
            result.setPredictedRating(eventLogService.getAverageRating(events));
        }
        return result;
    }
}
