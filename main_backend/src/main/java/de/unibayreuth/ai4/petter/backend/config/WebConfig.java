package de.unibayreuth.ai4.petter.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private final long MAX_AGE_SECS = 3600;


    @Autowired
    ObjectMapper objectMapper;

    @Value("${recpro.baseurl.dev}")
    String baseUrl;

    @Bean
    public WebMvcConfigurer corsDevConfigurer() {
        final String finalBaseUrl = this.baseUrl;
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                System.out.println(baseUrl);
//                Arrays.stream(finalBaseUrl).forEach(System.out::println);
                registry.addMapping("/**").allowedOrigins(finalBaseUrl)
                        .allowedMethods("PUT", "DELETE", "GET", "POST", "OPTIONS").maxAge(MAX_AGE_SECS);
            }

        };
    }
}
