package de.unibayreuth.ai4.petter.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KeycloakConfiguration {
    private String issuer;
    private String realm;
    private String clientId;
}
