package de.unibayreuth.ai4.petter.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Setter
@Getter
public class User {
    private String id; // = KecyloakID!
    private String firstName;
    private String lastName;
    private Set<String> attributeIds;

    public User(){
        this.attributeIds = new HashSet<>();
    }
}
