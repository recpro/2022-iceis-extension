package de.unibayreuth.ai4.petter.backend.model.bpmn;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Activity {

    private String id;
    private String name;
}
