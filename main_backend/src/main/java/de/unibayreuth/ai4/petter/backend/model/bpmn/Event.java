package de.unibayreuth.ai4.petter.backend.model.bpmn;

import de.unibayreuth.ai4.petter.backend.model.Rating;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Event {
    private String id;
    private Task task;
    private String status;
    private Instant startTime;
    private Instant endTime;
    private Rating rating;
}
