package de.unibayreuth.ai4.petter.backend.model.bpmn;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.tomcat.jni.Proc;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProcessInstance {

    private String id;
    private String description;
    private Process process;
}
