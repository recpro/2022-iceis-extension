package de.unibayreuth.ai4.petter.backend.model.bpmn;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Task {

    private String id;
    private Activity activity;
    private String data;
    private int priority;
    private String assigneeId;
    private String description;
    private ProcessInstance processInstance;
    private Instant createdAt;
}
