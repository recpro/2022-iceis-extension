package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import de.unibayreuth.ai4.petter.backend.model.recpro.RecproActivity;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproEventLog;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproWorklist;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContentBasedCalculation {
    List<String> attributeIds;
    Set<RecproActivity> activities;
    RecproEventLog eventLog;
    RecproWorklist worklist;
    String assigneeId;
}
