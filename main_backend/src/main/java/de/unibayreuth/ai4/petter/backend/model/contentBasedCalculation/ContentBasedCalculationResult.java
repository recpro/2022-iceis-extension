package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContentBasedCalculationResult {
    private String activityId;
    private double predictedRating;
    private int ratingCount;
    private double similarity;
    private String similarActivityId;
}
