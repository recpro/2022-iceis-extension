package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import de.unibayreuth.ai4.petter.backend.model.recpro.RecproActivity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ContentBasedResult {
    private String a;
    private String b;
    private double predictedRating;
    private int ratingCount;
    private double similarity;
}
