package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Distance {
    private Item a = new Item();
    private Item b = new Item();
    private double distance = 0;
}
