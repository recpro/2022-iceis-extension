package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import de.unibayreuth.ai4.petter.backend.model.bpmn.Activity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DistanceCalculation {

    private Activity a;
    private Activity b;
    private double distance;
}
