package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Item {
    private String id = "-1";
    private Vector<ItemVector> vector = new Vector<>();

}
