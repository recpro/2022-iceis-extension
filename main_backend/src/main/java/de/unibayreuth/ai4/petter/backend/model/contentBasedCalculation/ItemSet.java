package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ItemSet {
    private Item a = new Item();
    private Set<Item> items = new HashSet<>();
}
