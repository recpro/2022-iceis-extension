package de.unibayreuth.ai4.petter.backend.model.contentBasedCalculation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ItemVector {
    private String id = "";
    private double value = -1;
}
