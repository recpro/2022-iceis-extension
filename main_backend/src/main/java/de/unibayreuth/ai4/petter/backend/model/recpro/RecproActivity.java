package de.unibayreuth.ai4.petter.backend.model.recpro;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproActivity {

    private String id;
    private String name;
}
