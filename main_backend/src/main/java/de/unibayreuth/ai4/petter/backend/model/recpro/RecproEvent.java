package de.unibayreuth.ai4.petter.backend.model.recpro;

import de.unibayreuth.ai4.petter.backend.model.bpmn.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproEvent {

    private String id;
    private RecproTask task;
    private String status;
    private Instant startTime;
    private Instant endTime;
    private RecproRating rating;

}
