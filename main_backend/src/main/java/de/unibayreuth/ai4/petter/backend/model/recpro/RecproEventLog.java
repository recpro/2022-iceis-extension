package de.unibayreuth.ai4.petter.backend.model.recpro;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
public class RecproEventLog {
    private Set<RecproEvent> events;

    public RecproEventLog() {
        this.events = new HashSet<>();
    }
}
