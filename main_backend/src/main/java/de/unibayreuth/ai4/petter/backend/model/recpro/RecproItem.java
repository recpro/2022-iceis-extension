package de.unibayreuth.ai4.petter.backend.model.recpro;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproItem {
    private String id = "";
    private Vector<RecproItemVector> vector = new Vector<>();
}
