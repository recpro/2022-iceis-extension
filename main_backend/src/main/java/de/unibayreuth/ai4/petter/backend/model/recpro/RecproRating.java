package de.unibayreuth.ai4.petter.backend.model.recpro;

import de.unibayreuth.ai4.petter.backend.model.Rating;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproRating {

    private String id;
    private int rating;

    public RecproRating(Rating rating) {
        this.id = rating.getId();
        this.rating = rating.getRating();
    }

}
