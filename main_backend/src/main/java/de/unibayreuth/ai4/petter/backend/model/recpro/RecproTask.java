package de.unibayreuth.ai4.petter.backend.model.recpro;

import de.unibayreuth.ai4.petter.backend.bpms.ActivityService;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Process;
import de.unibayreuth.ai4.petter.backend.model.bpmn.ProcessInstance;
import de.unibayreuth.ai4.petter.backend.model.bpmn.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproTask {
    private String id;
    private RecproActivity activity;
    private String data;
    private int priority;
    private RecproUser assignee;
    private String description;
    private ProcessInstance processInstance;
    private Instant createdAt;

}
