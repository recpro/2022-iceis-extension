package de.unibayreuth.ai4.petter.backend.model.recpro;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproUser {
    private String id;
    private String firstName;
    private String lastName;
    private Set<RecproAttribute> attributes;
}
