package de.unibayreuth.ai4.petter.backend.model.recpro;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RecproWorklist {
    private String id;
    private Set<RecproTask> tasks;
}
