package de.unibayreuth.ai4.petter.backend.rating;

import de.unibayreuth.ai4.petter.backend.model.recpro.RecproRating;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RatingService {
    public RecproRating getById(String id) {
        return new RecproRating("0", 1);
    }

    public Set<RecproRating> getAll() {
        return new HashSet<>();
    }
}
