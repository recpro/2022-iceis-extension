package de.unibayreuth.ai4.petter.backend.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping(path = "/api")
public class TestController {

    @GetMapping(value = "/test")
    @RolesAllowed({"ROLE_ADMIN"})
    public SenderResponse test() {
        return new SenderResponse("Test from RS-Backend");
    }

    @GetMapping(value = "/demo")
    public SenderResponse adminEndPoint() {
        return new SenderResponse("Hello From Admin");
    }

    @GetMapping(value = "/manager")
    public SenderResponse managerEndPoint() {
        return new SenderResponse("Hello From Manager");
    }
}
