package de.unibayreuth.ai4.petter.backend.user;

import de.unibayreuth.ai4.petter.backend.model.recpro.RecproUser;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(path="test")
    public SenderResponse test() {
        return userService.test();
    }


    @GetMapping(path = "/getAll")
    @CrossOrigin(origins = "http://localhost:4200")
    @RolesAllowed("ROLE_ADMIN")
    public ResponseEntity<Set<RecproUser>> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping(path = "/getById")
    public ResponseEntity<RecproUser> getById(@RequestParam(name = "id") String id) {
        return ResponseEntity.ok(userService.getById(id));
    }
}
