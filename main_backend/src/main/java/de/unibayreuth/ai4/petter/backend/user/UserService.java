package de.unibayreuth.ai4.petter.backend.user;

import de.unibayreuth.ai4.petter.backend.ObjectCreationHelper;
import de.unibayreuth.ai4.petter.backend.model.User;
import de.unibayreuth.ai4.petter.backend.model.recpro.RecproUser;
import de.unibayreuth.ai4.petter.backend.test.SenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

@Service
@PropertySource("classpath:application.properties")
public class UserService {

    @Value("${recpro.ums.url}")
    private String url;

    public SenderResponse test() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/user/test", SenderResponse.class);
    }

    public Set<RecproUser> getAll() {
        Set<RecproUser> result = new HashSet<>();
        ObjectCreationHelper objectCreationHelper = new ObjectCreationHelper();
        this.getAllFromSource().forEach(u -> result.add(objectCreationHelper.getRecproUser(u)));
        return result;
    }


    private Set<User> getAllFromSource() {
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Set<User>> result = restTemplate.exchange(
                url + "/user/getAll",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {});
        return result.getBody();
    }

    public RecproUser getById(String id) {
        ObjectCreationHelper objectCreationHelper = new ObjectCreationHelper();
        return objectCreationHelper.getRecproUser(this.getByIdFromSource(id));
    }

    public User getByIdFromSource(String id) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url + "/user/getById?id=" + id, User.class);
//        return result.getId() == null ? new User() : result;
    }
}
