import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FullLayoutComponent} from "./components/layout/full-layout/full-layout.component";
import {Full_ROUTES} from "./components/layout/routes/full-layout.routes";
import {ContentLayoutComponent} from "./components/layout/content-layout/content-layout.component";
import {CONTENT_ROUTES} from "./components/layout/routes/content-layout.routes";

const routes: Routes = [

  {
    path: '',
    redirectTo: 'dashboard/default',
    pathMatch: 'full',
  },
  { path: '', component: FullLayoutComponent, data: { title: 'full Views'}, children: Full_ROUTES},
  { path: '', component: ContentLayoutComponent, data: { title: 'content Views' }, children: CONTENT_ROUTES },
  { path: '**', redirectTo: 'dashboard' },
  // {
  //   path: 'access-denied',
  //   component: AccessDeniedComponent,
  //   canActivate: [AuthGuard],
  // },d
  // {
  //   path: 'admin',
  //   component: AdminComponent,
  //   canActivate: [AuthGuard],
  //   // The user need to have this roles to access
  //   data: { roles: ['ROLE_ADMIN'] },
  // },
  // {
  //   path: 'manager',
  //   component: ManagerComponent,
  //   canActivate: [AuthGuard],
  //   data: { roles: ['ROLE_MANAGER'] },
  // },
  // {
  //   path: 'bpms',
  //   component: BpmsComponent,
  //   // canActivate: [AuthGuard],
  // },
  // {
  //   path: 'ums',
  //   component: UmsComponent,
  //   canActivate: [AuthGuard],
  // },
  // {
  //   path: 'recpro',
  //   component: RecproComponent,
  //   canActivate: [AuthGuard],
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
