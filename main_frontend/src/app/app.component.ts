import { Component } from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./util/config.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  constructor(
    private keycloakService: KeycloakService,
    private http: HttpClient,
    private configService: ConfigService
  ) {}
  logout() {
    this.keycloakService.logout( `${window.location.protocol}//${window.location.host}`);
  }

  test() {
    this.http.get(this.configService.config.apiUrl + '/api/bpms/worklist/test').subscribe((data) => {
      console.log(data);
      // this.message = data.message;
    });
  }
}
