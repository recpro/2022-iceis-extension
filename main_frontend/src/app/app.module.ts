import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import { AdminComponent } from './components/admin/admin.component';
import { ManagerComponent } from './components/manager/manager.component';
import {RuntimeInformationService} from "../util/runtime-information.service";
import {KeycloakConfiguration} from "./model/keycloakConfiguration";
import {BarRatingModule} from "ngx-bar-rating";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {LayoutModule} from "./components/layout/layout.module";
import {BpmsModule} from "./components/bpms/bpms.module";
import {UmsModule} from "./components/ums/ums.module";
import {RecproModule} from "./components/recpro/recpro.module";
import {AttributesModule} from "./components/attributes/attributes.module";
import {SettingsModule} from "./components/settings/settings.module";
import {AccessDeniedModule} from "./components/access-denied/access-denied.module";
import {WelcomeModule} from "./components/welcome/welcome.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatPaginatorModule} from "@angular/material/paginator";
import {LoadingInterceptor} from "./components/layout/spinner/loading.interceptor";
import {ConfigService} from "./util/config.service";

export const configFactory = (configService: ConfigService) => {
  return () => configService.loadConfig();
}

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    ManagerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    KeycloakAngularModule,
    HttpClientModule,
    BarRatingModule,
    PerfectScrollbarModule,
    LayoutModule,
    BpmsModule,
    UmsModule,
    RecproModule,
    AttributesModule,
    SettingsModule,
    AccessDeniedModule,
    WelcomeModule,
    BrowserAnimationsModule,
    MatPaginatorModule
  ],
  providers: [
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: initializer,
    //   deps: [KeycloakService],
    //   multi: true
    // }
    {
      provide: APP_INITIALIZER,
      useFactory: configurationFactory,
      multi: true,
      deps: [HttpClient, KeycloakService, RuntimeInformationService]
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: configFactory,
      deps: [ConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// @ts-ignore
function initializeKeycloak(keycloak: KeycloakService, keycloakConfig: KeycloakConfiguration ): Promise<boolean> {
  return keycloak.init({
    config: {
      // @ts-ignore
      url: keycloakConfig.issuer,
      // @ts-ignore
      realm: keycloakConfig.realm,
      // @ts-ignore
      clientId: keycloakConfig.clientId
    },
    initOptions: {
      flow: 'standard',
      onLoad: 'check-sso',
      // silentCheckSsoRedirectUri:
      //   window.location.origin + '/assets/silent-check-sso.html',
      checkLoginIframe: true,
      checkLoginIframeInterval: 30
    },
    loadUserProfileAtStartUp: true, // loads user information whenever keycloak is used for authentication
  });
}

export function configurationFactory(
  http: HttpClient,
  keycloakService: KeycloakService,
  runtime: RuntimeInformationService
): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      runtime.authUrl
        .toPromise()
        .then(data => {
          console.log(data);
          // If multiple async requests are necessary, we can just call them here one after another,
          // collect them in an array and return Promise.all(<the array>).
          // @ts-ignore
          return initializeKeycloak(keycloakService, data?.keycloak);
        })
        .then(() => {
          // Once configuration dependencies are resolved, then resolve factory
          resolve(true);
        })
        .catch(err => {
          console.log('An error occurred while loading the app configuration.', err);
          reject();
        });
    });
  };
}


