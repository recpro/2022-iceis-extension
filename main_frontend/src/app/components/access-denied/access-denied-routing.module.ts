import { NgModule } from '@angular/core';
import {AccessDeniedComponent} from "./access-denied.component";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: '',
    component: AccessDeniedComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccessDeniedRoutingModule { }
