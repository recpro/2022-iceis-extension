
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import {ConfigService} from "../../util/config.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  message = 'message';

  constructor(
    private http: HttpClient,
    private configService: ConfigService) {}

  ngOnInit(): void {

    this.http.get(this.configService.config.apiUrl + '/api/demo').subscribe(() => {
      console.log('SUCCESS');
      // this.message = data.message;
    });

  }

}
