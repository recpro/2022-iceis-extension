import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributesComponent } from './attributes.component';
import {HttpClientModule} from "@angular/common/http";
import {AuthService} from "../authentication/auth.service";

describe('AttributesComponent', () => {
  let component: AttributesComponent;
  let fixture: ComponentFixture<AttributesComponent>;

  let authService: jasmine.SpyObj<AuthService>;

  beforeEach(async () => {
    const authServiceSpy = jasmine.createSpyObj<AuthService>('AuthService', ['getAccess']);
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        { provide: AuthService, useValue: authServiceSpy }
      ],
      declarations: [ AttributesComponent ]
    })
    .compileComponents();
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;

    fixture = TestBed.createComponent(AttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
