import { Component, OnInit } from '@angular/core';
import {RecproAttribute} from "../bpms/model/RecproAttribute";
import {AttributesService} from "./attributes.service";
import {AuthService} from "../authentication/auth.service";
import {Role} from "../authentication/Role";

@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css']
})
export class AttributesComponent implements OnInit {

  attributes: RecproAttribute[] = [];

  addAttributeAccess = false;
  editAttributeAccess = false;

  constructor(private service: AttributesService, private authService: AuthService) { }

  ngOnInit(): void {
    this.service.getAll().subscribe((res: RecproAttribute[]) => {
      console.log(res);
      this.attributes = res.sort((a, b) => a.name.localeCompare(b.name));
    });
    this.setAccess();
  }

  private setAccess() {
    this.addAttributeAccess = this.authService.getAccess([Role.ADMIN]);
    this.editAttributeAccess = this.authService.getAccess([Role.ADMIN]);
  }

  editAttribute() {
    console.log('edit');
  }

  deleteAttribute() {
    console.log('delete');
  }

}
