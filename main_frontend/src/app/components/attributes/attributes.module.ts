import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AttributesComponent} from "./attributes.component";
import {AttributesRoutingModule} from "./attributes-routing.module";



@NgModule({
  declarations: [
    AttributesComponent
  ],
  imports: [
    CommonModule,
    AttributesRoutingModule
  ]
})
export class AttributesModule { }
