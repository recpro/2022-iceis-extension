import { TestBed } from '@angular/core/testing';

import { AttributesService } from './attributes.service';
import {HttpClientModule} from "@angular/common/http";

describe('AttributesService', () => {
  let service: AttributesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(AttributesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
