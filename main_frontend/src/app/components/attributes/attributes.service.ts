import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, share} from "rxjs";
import {RecproAttribute} from "../bpms/model/RecproAttribute";
import {ConfigService} from "../../util/config.service";

@Injectable({
  providedIn: 'root'
})
export class AttributesService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    ) { }

  getAll(): Observable<RecproAttribute[]> {
    return this.http.get<RecproAttribute[]>(this.configService.config.apiUrl + '/api/attribute/getAll').pipe(share());
  }
}
