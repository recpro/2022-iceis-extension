export enum Role {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
  PROCESS_OWNER = 'ROLE_PROCESS_OWNER'
}
