import { Injectable } from '@angular/core';
import {Role} from "./Role";
import {KeycloakService} from "keycloak-angular";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  ADMIN = Role.ADMIN;
  USER = Role.USER;
  PROCESS_OWNER = Role.PROCESS_OWNER;


  constructor(
    private keycloak: KeycloakService
  ) { }

  getUserRoles(): String[] {
    return this.keycloak.getUserRoles(false);
  }

  getAccess(roles: String[]): boolean {
    return roles.some(role => this.getUserRoles().includes(role));
  }

}
