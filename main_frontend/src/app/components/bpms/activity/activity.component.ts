import { Component, OnInit } from '@angular/core';
import {BpmsService} from "../bpms.service";
import {RecproActivity} from "../model/RecproActivity";

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  activities: RecproActivity[] = [];

  list: any[] = [];

  constructor(
    private service: BpmsService
  ) { }

  ngOnInit(): void {
    this.initActivities();
  }

  initActivities() {
    this.service.getActivities().subscribe((res: RecproActivity[]) => {
      this.activities = res.sort((a, b) => a.name.localeCompare(b.name));
      console.log(this.activities);
    });
  }

}
