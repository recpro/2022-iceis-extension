import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {BpmsComponent} from "./bpms.component";
import {AuthGuard} from "../../../util/auth.guard";
import {EventlogComponent} from "./eventlog/eventlog.component";
import {WorklistComponent} from "./worklist/worklist.component";
import {ActivityComponent} from "./activity/activity.component";

const routes: Routes = [
  {
    path: '',
    component: BpmsComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'eventlog',
    component: EventlogComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'worklist',
    component: WorklistComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'activities',
    component: ActivityComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BpmsRoutingModule { }
