import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BpmsComponent} from "./bpms.component";
import {BpmsRoutingModule} from "./bpms-routing.module";
import {ActivityComponent} from "./activity/activity.component";
import {EventlogComponent} from "./eventlog/eventlog.component";
import {WorklistComponent} from "./worklist/worklist.component";
import { TaskComponent } from './task/task.component';
import {MatInputModule} from "@angular/material/input";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import { EventDialogComponent } from './eventlog/event-dialog/event-dialog.component';
import {MatCardModule} from "@angular/material/card";
import { ActivityDialogComponent } from './activity/activity-dialog/activity-dialog.component';
import { WorklistDialogComponent } from './worklist/worklist-dialog/worklist-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";



@NgModule({
  declarations: [
    BpmsComponent,
    ActivityComponent,
    EventlogComponent,
    WorklistComponent,
    TaskComponent,
    EventDialogComponent,
    ActivityDialogComponent,
    WorklistDialogComponent,
  ],
    exports: [
    ],
  imports: [
    CommonModule,
    BpmsRoutingModule,
    MatInputModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ]
})
export class BpmsModule { }
