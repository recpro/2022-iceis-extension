import { TestBed } from '@angular/core/testing';

import { BpmsService } from './bpms.service';
import {HttpClientModule} from "@angular/common/http";

describe('BpmsService', () => {
  let service: BpmsService;

  beforeEach(() => {
    TestBed.configureTestingModule({

      imports: [HttpClientModule],
    });
    service = TestBed.inject(BpmsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
