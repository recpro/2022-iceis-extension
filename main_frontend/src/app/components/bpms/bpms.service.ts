import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable, share} from "rxjs";
import {RecproEventLog} from "./model/RecproEventLog";
import {RecproWorklist} from "./model/RecproWorklist";
import {RecproActivity} from "./model/RecproActivity";
import {ConfigService} from "../../util/config.service";

@Injectable({
  providedIn: 'root'
})
export class BpmsService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) { }

  getEventLog(): Observable<RecproEventLog> {
    return this.http.get<RecproEventLog>(this.configService.config.apiUrl + '/api/bpms/eventlog/getEventLog').pipe(share());
  }

  getWorklist(): Observable<RecproWorklist> {
    return this.http.get<RecproWorklist>(this.configService.config.apiUrl + '/api/bpms/worklist/getWorklist').pipe(share());
  }

  getActivities(): Observable<RecproActivity[]> {
    return this.http.get<RecproActivity[]>(this.configService.config.apiUrl + '/api/bpms/activity/getAll').pipe(share());
  }

}
