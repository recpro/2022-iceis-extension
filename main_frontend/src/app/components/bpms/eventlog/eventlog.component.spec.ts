import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventlogComponent } from './eventlog.component';
import {HttpClientModule} from "@angular/common/http";

describe('EventlogComponent', () => {
  let component: EventlogComponent;
  let fixture: ComponentFixture<EventlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ EventlogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EventlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
