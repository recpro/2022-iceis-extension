import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {BpmsService} from "../bpms.service";
import {RecproEventLog} from "../model/RecproEventLog";
import {MatTableDataSource} from "@angular/material/table";
import {RecproEvent} from "../model/RecproEvent";
import {MatPaginator, MatPaginatorIntl} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {RecproTask} from "../model/RecproTask";
import {RecproRating} from "../model/RecproRating";

@Component({
  selector: 'app-eventlog',
  templateUrl: './eventlog.component.html',
  styleUrls: ['./eventlog.component.css']
})
export class EventlogComponent implements OnInit, AfterViewInit {

  eventLog: RecproEventLog = new RecproEventLog([]);

  displayedColumns = ["ID", "activity", "status", "rating", "assignee", "details"];
  dataSource: MatTableDataSource<RecproEvent> = new MatTableDataSource<RecproEvent>();

  @ViewChild(MatPaginator) paginator: MatPaginator = new MatPaginator(new MatPaginatorIntl(), ChangeDetectorRef.prototype);
  @ViewChild(MatSort) sort: MatSort = new MatSort();



  constructor(private bpmsService: BpmsService) {
    this.bpmsService.getEventLog().subscribe((res: RecproEventLog) => {
      console.log(res);
      this.eventLog = res;
      this.dataSource = new MatTableDataSource<RecproEvent>(res.events);
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = (data: any, filter) => {
        const dataStr =JSON.stringify(data).toLowerCase();
        return dataStr.indexOf(filter) != -1;
      }
      this.dataSource.sort = this.sort;
    });
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(eventTarget: EventTarget | null) {
    if (eventTarget) {
      let filterValue = (eventTarget as HTMLInputElement).value.trim();
      filterValue = filterValue.toLowerCase();
      console.log(filterValue);
      this.dataSource.filter = filterValue;
    }
  }

}
