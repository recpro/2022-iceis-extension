
export class RecproAttribute {
  id: string;
  name: string;
  description: string;
  activityIds: string[] = [];
  userIds: string[] = [];
  taskIds: string[] = [];
  processIds: string[] = [];
  eventIds: string[] = [];

  constructor(id: string, name: string, description: string) {
    this.id = id;
    this.name = name;
    this.description = description;
  }


}
