import {RecproTask} from "./RecproTask";
import {RecproRating} from "./RecproRating";

export class RecproEvent {
  id: string;
  task: RecproTask;
  status: string;
  startTime: Date;
  endTime: Date;
  rating: RecproRating;

  constructor(id: string, task: RecproTask, status: string, startTime: Date, endTime: Date, rating: RecproRating) {
    this.id = id;
    this.task = task;
    this.status = status;
    this.startTime = startTime;
    this.endTime = endTime;
    this.rating = rating;
  }
}
