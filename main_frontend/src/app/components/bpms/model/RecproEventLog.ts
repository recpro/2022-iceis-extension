import {RecproEvent} from "./RecproEvent";

export class RecproEventLog {
  events: RecproEvent[];

  constructor(events: RecproEvent[]) {
    this.events = events;
  }
}
