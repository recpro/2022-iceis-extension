import {RecproActivity} from "./RecproActivity";

export class RecproProcess {
  id: string;
  name: string;
  description: string;
  activities: RecproActivity[];

  constructor(id: string, name: string, description: string, activities: RecproActivity[]) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.activities = activities;
  }
}
