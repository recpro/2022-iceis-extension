import {RecproProcess} from "./RecproProcess";

export class RecproProcessInstance {
  id: string;
  description: string;
  process: RecproProcess

  constructor(id: string, description: string, process: RecproProcess) {
    this.id = id;
    this.description = description;
    this.process = process;
  }
}
