export class RecproRating {
  id: string;
  rating: number;

  constructor(id: string, rating: number) {
    this.id = id;
    this.rating = rating;
  }
}
