import {RecproActivity} from "./RecproActivity";
import {RecproAttribute} from "./RecproAttribute";
import {RecproUser} from "../../ums/model/RecproUser";

export class RecproTask {
  id: string;
  activity: RecproActivity;
  attributes: RecproAttribute[];
  data: string;
  priority: number;
  assignee: RecproUser;
  description: string;
  processInstanceId: string;
  createdAt: Date;
  processId: string;

  constructor(id: string, activity: RecproActivity, attributes: RecproAttribute[], data: string, priority: number,
              assignee: RecproUser, description: string, processInstanceId: string, createdAt: Date, processId: string) {
    this.id = id;
    this.activity = activity;
    this.attributes = attributes;
    this.data = data;
    this.priority = priority;
    this.assignee = assignee;
    this.description = description;
    this.processInstanceId = processInstanceId;
    this.createdAt = createdAt;
    this.processId = processId;
  }
}
