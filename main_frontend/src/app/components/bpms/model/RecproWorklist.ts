import {RecproTask} from "./RecproTask";

export class RecproWorklist {
  tasks: RecproTask[];

  constructor(tasks: RecproTask[]) {
    this.tasks = tasks;
  }
}
