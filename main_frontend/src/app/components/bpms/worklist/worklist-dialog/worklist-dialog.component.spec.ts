import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorklistDialogComponent } from './worklist-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

describe('WorklistDialogComponent', () => {
  let component: WorklistDialogComponent;
  let fixture: ComponentFixture<WorklistDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorklistDialogComponent ],
      providers: [
        {provide: MAT_DIALOG_DATA, useValue: {}},
        { provide: MatDialogRef, useValue: {} }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorklistDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
