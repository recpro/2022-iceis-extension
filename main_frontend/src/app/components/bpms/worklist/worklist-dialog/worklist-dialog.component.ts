import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {RecproTask} from "../../model/RecproTask";

@Component({
  selector: 'app-worklist-dialog',
  templateUrl: './worklist-dialog.component.html',
  styleUrls: ['./worklist-dialog.component.css']
})
export class WorklistDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public task: RecproTask
  ) { }

  ngOnInit(): void {
  }

}
