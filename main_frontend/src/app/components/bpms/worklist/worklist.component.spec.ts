import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorklistComponent } from './worklist.component';
import {HttpClientModule} from "@angular/common/http";
import {KeycloakService} from "keycloak-angular";
import {WorklistDialogComponent} from "./worklist-dialog/worklist-dialog.component";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";

describe('WorklistComponent', () => {
  let component: WorklistComponent;
  let fixture: ComponentFixture<WorklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatDialogModule
      ],
      declarations: [ WorklistComponent ],
      providers: [
        KeycloakService,
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
