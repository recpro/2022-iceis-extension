import { Component, OnInit } from '@angular/core';
import {BpmsService} from "../bpms.service";
import {RecproWorklist} from "../model/RecproWorklist";
import {RecproService} from "../../recpro/recpro.service";
import {RecproTask} from "../model/RecproTask";
import {ContentBasedCalculationResult} from "../../recpro/model/ContentBasedCalculationResult";
import {MatDialog} from "@angular/material/dialog";
import {WorklistDialogComponent} from "./worklist-dialog/worklist-dialog.component";
import {LoaderService} from "../../layout/spinner/loader.service";

@Component({
  selector: 'app-worklist',
  templateUrl: './worklist.component.html',
  styleUrls: ['./worklist.component.css']
})
export class WorklistComponent implements OnInit {

  worklist: RecproWorklist = new RecproWorklist([]);
  contentBasedCalculationResult: ContentBasedCalculationResult[] = [];
  sortOrder = 'fifo';

  displayed: any[] = [];

  constructor(
    private bpmsService: BpmsService,
    private recproService: RecproService,
    public dialog: MatDialog,
    private loader: LoaderService
    ) { }

  ngOnInit(): void {
    this.initWorklist();
  }

  initWorklist() {
    this.bpmsService.getWorklist().subscribe((res: RecproWorklist) => {
      this.worklist = res;
      // this.displayed = res;
      console.log(res);
      this.initList();
    });
  }

  sort(type: string) {
    console.log('SORT: ' + type);
    this.sortOrder = type;
    switch (type) {
      case 'contentBased':
        if (this.contentBasedCalculationResult.length !== this.worklist.tasks.length) {
          this.contentBasedCalculation();
        }
        this.displayed = this.displayed.sort((a,b) => b.priority - a.priority || b.predictedRating - a.predictedRating || new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
        break;
      default:
        this.displayed = this.displayed.sort((a,b) =>b.priority - a.priority || new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
    }
  }

  viewDetails(task: RecproTask) {
    this.dialog.open(WorklistDialogComponent, {
      data: task
    });
  }

  contentBasedCalculation() {
    this.loader.setLoadingAndMessage(true, 'Calculating...');
    this.recproService.getCalculation().subscribe(result => {
      console.log(result)
      this.contentBasedCalculationResult = result;
      this.initList();
    });
  }

  initList() {
    this.displayed = [];
    this.worklist.tasks.forEach((task: RecproTask) => {
      let calc = this.contentBasedCalculationResult[this.contentBasedCalculationResult.findIndex(cbcr => cbcr.a === task.activity.id)];
      this.displayed.push({...task, ...calc});
      console.log(this.displayed);
      this.sort(this.sortOrder);
    });
  }
}
