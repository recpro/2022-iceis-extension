import { Component, OnInit } from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {AuthService} from "../authentication/auth.service";
import {Role} from "../authentication/Role";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  diagramUrl = 'https://cdn.staticaly.com/gh/bpmn-io/bpmn-js-examples/dfceecba/starter/diagram.bpmn';
  importError?: Error;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.getAccess([Role.ADMIN]);
  }

  handleImported(event: { type: any; error: any; warnings: any; }) {

    const {
      type,
      error,
      warnings
    } = event;

    if (type === 'success') {
      console.log(`Rendered diagram (%s warnings)`, warnings.length);
    }

    if (type === 'error') {
      console.error('Failed to render diagram', error);
    }

    this.importError = error;
  }
}
