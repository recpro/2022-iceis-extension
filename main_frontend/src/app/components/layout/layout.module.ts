import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FooterComponent} from "./footer/footer.component";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {RouterModule} from "@angular/router";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {NavbarComponent} from "./navbar/navbar.component";
import {ContentLayoutComponent} from "./content-layout/content-layout.component";
import {FullLayoutComponent} from "./full-layout/full-layout.component";
import {KeycloakAngularModule} from "keycloak-angular";
import { SpinnerComponent } from './spinner/spinner/spinner.component';



@NgModule({
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    ContentLayoutComponent,
    FullLayoutComponent,
    SpinnerComponent,
  ],
    exports: [
        CommonModule,
        FooterComponent,
        SidebarComponent,
        ContentLayoutComponent,
        FullLayoutComponent,
        NgbModule,
        SpinnerComponent
    ],
  imports: [
    RouterModule,
    CommonModule,
    NgbModule,
    PerfectScrollbarModule,
    KeycloakAngularModule
  ]
})
export class LayoutModule { }
