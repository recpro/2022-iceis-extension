import { Component, OnInit } from '@angular/core';
import {SidebarService} from "../sidebar/sidebar.service";
import {KeycloakService} from "keycloak-angular";
import {UmsService} from "../../ums/ums.service";
import {KeycloakProfile} from "keycloak-js";
import {RecproUser} from "../../ums/model/RecproUser";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: boolean = false;

  currentKeycloakUser: KeycloakProfile = this.umsService.currentKeycloakUser;
  initials: String = '';
  currentDbUser: RecproUser = new RecproUser('', '', '', []);

  constructor(
    public sidebarservice: SidebarService,
    private keycloakService: KeycloakService,
    private umsService: UmsService
  ) { }

  toggleSidebar() {
    this.sidebarservice.setSidebarState(!this.sidebarservice.getSidebarState());
  }

  getSideBarState() {
    return this.sidebarservice.getSidebarState();
  }

  hideSidebar() {
    this.sidebarservice.setSidebarState(true);
  }

  logout() {
    this.keycloakService.logout( `${window.location.protocol}//${window.location.host}`);
  }

  login() {
    this.keycloakService.login();
  }

  ngOnInit() {
    /* Search Bar */
    $(document).ready(function () {
      $(".mobile-search-icon").on("click", function () {
        $(".search-bar").addClass("full-search-bar")
      }),
        $(".search-close").on("click", function () {
          $(".search-bar").removeClass("full-search-bar")
        })
    });

    this.keycloakService.isLoggedIn().then(res => {
      this.isLoggedIn = res;

      if (this.isLoggedIn) {
      this.umsService.getCurrentKeycloakUser().then(() => {
        this.currentKeycloakUser = this.umsService.currentKeycloakUser;
        this.umsService.getCurrentDbUser().subscribe((res: RecproUser) => {
          this.currentDbUser = res;
          this.initials = this.umsService.getInitials(this.currentKeycloakUser);
        });
      });
      }
    });

  }
}
