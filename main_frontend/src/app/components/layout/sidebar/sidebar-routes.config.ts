import { RouteInfo } from './sidebar.metadata';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [

    {
        path: '/dashboard', title: 'Dashboard', icon: 'bx bxs-dashboard', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [
      ]
    },
    {
      path: '', title: 'BPMS', icon: 'bx bx-shape-circle', class: 'sub', badge: '', badgeClass: '', isExternalLink: false, submenu: [
        {path: '/bpms/eventlog', title: 'EventLog', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []},
        {path: '/bpms/worklist', title: 'Worklist', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []},
        {path: '/bpms/activities', title: 'Activities', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []}
      ]
    },
    {
      path: '/attributes', title: 'ATTRIBUTES', icon: 'bx bx-bar-chart', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
      path: '/recpro', title: 'CALCULATION', icon: 'bx bx-line-chart', class: 'sub', badge: '', badgeClass: '', isExternalLink: false, submenu: [
            {path: '/recpro/calculation/contentBased', title: 'Contend-based', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []}
      ]
    }
];
