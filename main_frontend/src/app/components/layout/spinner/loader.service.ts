import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private loading: boolean = false;
  private message: string = 'Loading..';

  constructor() { }

  setLoading(loading: boolean) {
    this.loading = loading;
    if (!this.loading) {
      this.message = 'Loading...';
    }
  }

  setLoadingAndMessage(loading: boolean, message: string) {
    this.loading = loading;
    this.message = message;
  }

  getLoading(): boolean {
    return this.loading;
  }

  getMessage(): string {
    return this.message;
  }
}
