
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import {ConfigService} from "../../util/config.service";

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  message = 'message';

  constructor(private http: HttpClient,
              private configService: ConfigService) {}

  ngOnInit(): void {
    this.http.get(this.configService.config.apiUrl + '/api/manager').subscribe((data: any) => {
      this.message = data.message;
    });
  }
}
