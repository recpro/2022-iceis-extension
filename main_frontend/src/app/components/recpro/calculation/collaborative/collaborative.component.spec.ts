import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborativeComponent } from './collaborative.component';

describe('CollaborativeComponent', () => {
  let component: CollaborativeComponent;
  let fixture: ComponentFixture<CollaborativeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaborativeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CollaborativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
