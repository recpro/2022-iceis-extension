import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentBasedComponent } from './content-based.component';
import {HttpClientModule} from "@angular/common/http";
import {KeycloakService} from "keycloak-angular";

describe('ContentBasedComponent', () => {
  let component: ContentBasedComponent;
  let fixture: ComponentFixture<ContentBasedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ ContentBasedComponent ],
      providers: [
        KeycloakService
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContentBasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
