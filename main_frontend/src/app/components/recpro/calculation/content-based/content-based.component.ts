import { Component, OnInit } from '@angular/core';
import {RecproService} from "../../recpro.service";
import {DistanceCalculation} from "../../model/DistanceCalculation";
import {BpmsService} from "../../../bpms/bpms.service";
import {RecproActivity} from "../../../bpms/model/RecproActivity";

@Component({
  selector: 'app-content-based',
  templateUrl: './content-based.component.html',
  styleUrls: ['./content-based.component.css']
})
export class ContentBasedComponent implements OnInit {

  distances: DistanceCalculation[] = [];
  activities: RecproActivity[] = [];

  constructor(
    private recproService: RecproService,
    private bpmsService: BpmsService
  ) { }

  ngOnInit(): void {
    this.getCalculation();
    this.getActivities();
  }

  getCalculation() {
    this.recproService.getAllDistances().subscribe(res => {
      this.distances = res;
    })
  }

  getActivities() {
    this.bpmsService.getActivities().subscribe(res  => {
      this.activities = res;
    });
  }

  getCalculationForActivities(a: RecproActivity, b: RecproActivity) {
    let distance = this.distances.find(d => d.a.id === a.id && d.b.id === b.id)
    return distance === undefined ? -1 : distance.distance;
  }

}
