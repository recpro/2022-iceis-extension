import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KnowledgeBasedComponent } from './knowledge-based.component';

describe('KnowledgeBasedComponent', () => {
  let component: KnowledgeBasedComponent;
  let fixture: ComponentFixture<KnowledgeBasedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KnowledgeBasedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KnowledgeBasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
