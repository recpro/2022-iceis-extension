import {RecproActivity} from "../../bpms/model/RecproActivity";

export class ContentBasedCalculationResult {
  a: string;
  predictedRating: number;
  ratingCount: number;
  similarity: number;
  b: string;

  constructor(a: string, predictedRating: number, ratingCount: number, similarity: number, b: string) {
    this.a = a;
    this.predictedRating = predictedRating;
    this.ratingCount = ratingCount;
    this.similarity = similarity;
    this.b = b;
  }
}
