import {RecproActivity} from "../../bpms/model/RecproActivity";

export class DistanceCalculation {

  a: RecproActivity;
  b: RecproActivity;
  distance: number;

  constructor(a: RecproActivity, b: RecproActivity, distance: number) {
    this.a = a;
    this.b = b;
    this.distance = distance;
  }

}
