import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecproConfigComponent } from './recpro-config.component';

describe('RecproConfigComponent', () => {
  let component: RecproConfigComponent;
  let fixture: ComponentFixture<RecproConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecproConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecproConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
