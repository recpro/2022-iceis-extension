import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {RecproComponent} from "./recpro.component";
import {AuthGuard} from "../../../util/auth.guard";
import {ContentBasedComponent} from "./calculation/content-based/content-based.component";
import {KnowledgeBasedComponent} from "./calculation/knowledge-based/knowledge-based.component";
import {CollaborativeComponent} from "./calculation/collaborative/collaborative.component";
import {HybridComponent} from "./calculation/hybrid/hybrid.component";

const routes: Routes = [
  {
    path: '',
    component: RecproComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'calculation/contentBased',
    component: ContentBasedComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'calculation/knowledgeBased',
    component: KnowledgeBasedComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'calculation/collaborative',
    component: CollaborativeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  },
  {
    path: 'calculation/hybrid',
    component: HybridComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RecproRoutingModule { }
