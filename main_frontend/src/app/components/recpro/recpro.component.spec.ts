import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecproComponent } from './recpro.component';

describe('RecproComponent', () => {
  let component: RecproComponent;
  let fixture: ComponentFixture<RecproComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecproComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecproComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
