import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RecproComponent} from "./recpro.component";
import {RecproConfigComponent} from "./recpro-config/recpro-config.component";
import {RecproRoutingModule} from "./recpro-routing.module";
import { CalculationComponent } from './calculation/calculation.component';
import { ContentBasedComponent } from './calculation/content-based/content-based.component';
import { KnowledgeBasedComponent } from './calculation/knowledge-based/knowledge-based.component';
import { CollaborativeComponent } from './calculation/collaborative/collaborative.component';
import { HybridComponent } from './calculation/hybrid/hybrid.component';



@NgModule({
  declarations: [
    RecproComponent,
    RecproConfigComponent,
    CalculationComponent,
    ContentBasedComponent,
    KnowledgeBasedComponent,
    CollaborativeComponent,
    HybridComponent
  ],
  imports: [
    CommonModule,
    RecproRoutingModule
  ]
})
export class RecproModule { }
