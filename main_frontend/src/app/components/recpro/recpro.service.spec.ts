import {ComponentFixture, TestBed} from '@angular/core/testing';

import { RecproService } from './recpro.service';
import {HttpClientModule} from "@angular/common/http";
import {KeycloakService} from "keycloak-angular";
import {UmsService} from "../ums/ums.service";

describe('RecproService', () => {
  let service: RecproService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        KeycloakService
      ],
    });
    service = TestBed.inject(RecproService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
