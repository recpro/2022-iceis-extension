import { Injectable } from '@angular/core';
import {DistanceCalculation} from "./model/DistanceCalculation";
import {HttpClient} from "@angular/common/http";
import {share} from "rxjs";
import {ContentBasedCalculationResult} from "./model/ContentBasedCalculationResult";
import {UmsService} from "../ums/ums.service";
import {ConfigService} from "../../util/config.service";

@Injectable({
  providedIn: 'root'
})
export class RecproService {

  constructor(
    private http: HttpClient,
    private ums: UmsService,
    private configService: ConfigService
  ) { }

  getAllDistances() {
    return this.http.get<DistanceCalculation[]>(this.configService.config.apiUrl + '/api/calculation/allDistances').pipe(share());
  }

  getCalculation() {
    return this.http.get<ContentBasedCalculationResult[]>(this.configService.config.apiUrl + '/api/calculation/calculate?assigneeId=' + this.ums.currentKeycloakUser.id).pipe(share());
  }
}
