import {RecproAttribute} from "../../bpms/model/RecproAttribute";

export class RecproUser {
  id: String;
  firstName: String;
  lastName: String;
  attributes: RecproAttribute[];

  constructor(id: String, firstName: String, lastName: String, attributes: RecproAttribute[]) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.attributes = attributes;
  }
}
