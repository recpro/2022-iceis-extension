import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {UmsComponent} from "./ums.component";
import {AuthGuard} from "../../../util/auth.guard";

const routes: Routes = [
  {
    path: '',
    component: UmsComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_ADMIN'] }
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UmsRoutingModule { }
