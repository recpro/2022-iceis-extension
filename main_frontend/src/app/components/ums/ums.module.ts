import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UmsComponent} from "./ums.component";
import {UserComponent} from "./user/user.component";
import {UmsRoutingModule} from "./ums-routing.module";



@NgModule({
  declarations: [
    UmsComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    UmsRoutingModule
  ]
})
export class UmsModule { }
