import { TestBed } from '@angular/core/testing';

import { UmsService } from './ums.service';
import {HttpClientModule} from "@angular/common/http";
import {KeycloakService} from "keycloak-angular";

describe('UmsService', () => {
  let service: UmsService;

  beforeEach(() => {
    TestBed.configureTestingModule({

      imports: [HttpClientModule],
      providers: [KeycloakService],
    });
    service = TestBed.inject(UmsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
