import { Injectable } from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {KeycloakProfile} from "keycloak-js";
import {HttpClient} from "@angular/common/http";
import {Observable, share} from "rxjs";
import {RecproUser} from "./model/RecproUser";
import {environment} from "../../../environments/environment";
import {ConfigService} from "../../util/config.service";

@Injectable({
  providedIn: 'root'
})
export class UmsService {

  currentKeycloakUser: KeycloakProfile = new class implements KeycloakProfile {
    createdTimestamp: number = 0;
    email: string = "";
    emailVerified: boolean = false;
    enabled: boolean = false;
    firstName: string = "";
    id: string = "";
    lastName: string = "";
    totp: boolean = false;
    username: string = "";
  };

  constructor(
    private keycloakService: KeycloakService,
    private httpService: HttpClient,
    private configService: ConfigService) {
    // this.getCurrentKeycloakUser();
  }

  getCurrentKeycloakUser(): Promise<void | KeycloakProfile> {
    return this.keycloakService.loadUserProfile().then((res: KeycloakProfile) => {
      console.log(res);
      this.currentKeycloakUser = res;
    });
  }

  getKecyloakRoles(): String[] {
    return this.keycloakService.getUserRoles(false);
  }

  getCurrentDbUser(): Observable<RecproUser> {
    return this.httpService.get<RecproUser>(this.configService.config.apiUrl + '/api/user/getById?id=' + this.currentKeycloakUser.id).pipe(share());
  }

  getAllDbUsers(): Observable<RecproUser[]> {
    return this.httpService.get<RecproUser[]>(this.configService.config.apiUrl + '/api/user/getAll').pipe(share());
  }

  getInitials(keycloakUser: KeycloakProfile): String {
    let first = keycloakUser.firstName?.charAt(0);
    first == undefined ? first = '' : first = first;
    let last = keycloakUser.lastName?.charAt(0);
    last == undefined ? last = '' : last = last;
    return first + last;
  }
}
