import { Component, OnInit } from '@angular/core';
import {UmsService} from "../ums.service";
import {KeycloakProfile} from "keycloak-js";
import {RecproUser} from "../model/RecproUser";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  currentKeycloakUser: KeycloakProfile = this.service.currentKeycloakUser;
  currentDbUser: RecproUser = new RecproUser('', '', '', []);
  dbUsers: RecproUser[] = [];
  initials: String = '';

  constructor(private service: UmsService) { }

  ngOnInit(): void {
    this.service.getCurrentKeycloakUser().then(() => {
      this.currentKeycloakUser = this.service.currentKeycloakUser;
      this.service.getCurrentDbUser().subscribe((res: RecproUser) => {
        this.currentDbUser = res;
        this.initials = this.service.getInitials(this.currentKeycloakUser);
        this.service.getKecyloakRoles();
      });
    });

    console.log(this.currentKeycloakUser);
    this.service.getAllDbUsers().subscribe((res: RecproUser[]) => {
      console.log(res);
      this.dbUsers = res;
    });
  }

}
