import {KeycloakConfiguration} from "../model/keycloakConfiguration";

export class Config {
  apiUrl: string = '';
  production: boolean = false;
  serverUrl: string = '';
  keycloak: KeycloakConfiguration;

  constructor() {
    this.apiUrl = '';
    this.production = false;
    this.serverUrl = '';
    this.keycloak = new KeycloakConfiguration('', '', '');
  }
}
