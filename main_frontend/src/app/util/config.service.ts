import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Config } from './Config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  config: Config = new Config();

  constructor(
    private http: HttpClient
  ) { }

  loadConfig() {
    return this.http.get<Config>('./assets/config/config.json').subscribe(res => {
      console.log(res);
      this.config = res;
    });
  }
}
