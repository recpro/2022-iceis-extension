import { TestBed } from '@angular/core/testing';

import { RuntimeInformationService } from './runtime-information.service';
import {HttpClientModule} from "@angular/common/http";

describe('RuntimeInformationService', () => {
  let service: RuntimeInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(RuntimeInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
