import { Injectable } from '@angular/core';
import {Observable, share} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {KeycloakConfiguration} from "../app/model/keycloakConfiguration";
import {ConfigService} from "../app/util/config.service";
import { environment } from 'src/environments/environment';
import {Config} from "../app/util/Config";


@Injectable({
  providedIn: 'root'
})
export class RuntimeInformationService {
  private _authUrl!: Observable<KeycloakConfiguration>;
  private _cachedAuthUrl: KeycloakConfiguration | undefined;
  private _config!: Observable<Config>;

  constructor(
    private _http: HttpClient,
    private configService: ConfigService) { }

  // @ts-ignore
  get authUrl(): Observable<Config> {
    if (this._authUrl) {

    } else {
      // let absoluteHost  = 'http://localhost:8080';
      // if (!environment.production) {
      //   // In dev mode the backend host is different from the frontend host. In productive mode it's the same (frontend bundled in jar).
      //   absoluteHost = 'http://localhost:8080';
      // }
      // let absoluteHost  = this.configService.config.apiUrl;
      this._config = this._http.get<Config>('./assets/config/config.json').pipe(share());
      // this._authUrl = this._http.get<KeycloakConfiguration>(this.configService.config.apiUrl + '/api/runtime/authurl').pipe(share());
      // this._authUrl.subscribe(url => this._cachedAuthUrl = url);
      return this._config;
    }
  }
}
