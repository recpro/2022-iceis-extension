package de.unibayreuth.ai4.petter.contentbasedcalculation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContentBasedCalculationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContentBasedCalculationApplication.class, args);
	}

}
