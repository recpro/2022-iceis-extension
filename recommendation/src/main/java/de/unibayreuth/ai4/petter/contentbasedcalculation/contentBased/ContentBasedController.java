package de.unibayreuth.ai4.petter.contentbasedcalculation.contentBased;

import de.unibayreuth.ai4.petter.contentbasedcalculation.model.Distance;
import de.unibayreuth.ai4.petter.contentbasedcalculation.model.Item;
import de.unibayreuth.ai4.petter.contentbasedcalculation.model.ItemSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/contentBased/")
public class ContentBasedController {
    @Autowired
    ContentBasedService contentBasedService;

    @PostMapping(path = "/distance")
    public ResponseEntity<Distance> getDistance(@RequestBody ItemSet items) {
        return ResponseEntity.ok(contentBasedService.calculateDistance(items.getA(), items.getItems().stream().findFirst().orElse(new Item())));
    }

    @PostMapping(path = "/allDistancesForItem")
    public ResponseEntity<Set<Distance>> getAllDistancesForItem(@RequestBody ItemSet items) {
        return ResponseEntity.ok(contentBasedService.getAllDistancesForItem(items.getA(), items.getItems()));
    }

    @PostMapping(path = "/allDistances")
    public ResponseEntity<Set<Distance>> getAllDistances(@RequestBody ItemSet items) {
        return ResponseEntity.ok(contentBasedService.getAllDistances(items.getItems()));
    }

    @PostMapping(path = "/minDistance")
    public ResponseEntity<Distance> getMinDistance(@RequestBody ItemSet items) {
        return ResponseEntity.ok(contentBasedService.getMinimalDistance(items.getA(), items.getItems()));
    }

    @PostMapping(path = "/maxDistance")
    public ResponseEntity<Distance> getMaxDistance(@RequestBody ItemSet items) {
        return ResponseEntity.ok(contentBasedService.getMaximumDistance(items.getA(), items.getItems()));
    }
}
