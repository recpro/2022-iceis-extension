package de.unibayreuth.ai4.petter.contentbasedcalculation.contentBased;

import de.unibayreuth.ai4.petter.contentbasedcalculation.model.Distance;
import de.unibayreuth.ai4.petter.contentbasedcalculation.model.Item;
import de.unibayreuth.ai4.petter.contentbasedcalculation.model.ItemVector;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ContentBasedService {

    public Distance calculateDistance(Item a, Item b) {
        List<Double> test = new ArrayList<>();
        a.getVector().forEach(va -> test.add(Math.pow(va.getValue() - b.getVector().stream().filter(vb -> vb.getId().equals(va.getId())).findFirst().orElse(new ItemVector()).getValue(), 2.0)));

        double d = Math.sqrt(test.stream().reduce(0.0, Double::sum));
        double sim = (1 / (1 + d));
        return new Distance(a, b, sim);
    }

    public Set<Distance> getAllDistancesForItem(Item a, Set<Item> items) {
        Set<Distance> result = new HashSet<>();
        items.forEach(b -> result.add(calculateDistance(a, b)));
        return result;
    }

    public Set<Distance> getAllDistances(Set<Item> items) {
        Set<Distance> result = new HashSet<>();
        items.forEach(a -> items.forEach(b -> result.add(calculateDistance(a, b))));
        return result;
    }

    public Distance getMinimalDistance(Item a, Set<Item> items) {
        return this.getAllDistancesForItem(a, items).stream().reduce((x, y) -> x.getDistance() < y.getDistance() ? y : x).orElse(new Distance(a, a, 1));
    }

    public Distance getMaximumDistance(Item a, Set<Item> items) {
        return this.getAllDistancesForItem(a, items).stream().reduce((x, y) -> x.getDistance() > y.getDistance() ? y : x).orElse(new Distance(a, a, 1));
    }
}
