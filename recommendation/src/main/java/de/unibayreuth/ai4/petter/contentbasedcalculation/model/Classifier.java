package de.unibayreuth.ai4.petter.contentbasedcalculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Classifier {

    private String id;
    private String name;
    private String description;
    private Set<String> activityIds = new HashSet<>();
    private Set<String> userIds = new HashSet<>();
    private Set<String> taskIds = new HashSet<>();
    private Set<String> processIds = new HashSet<>();
    private Set<String> eventIds = new HashSet<>();
}
