package de.unibayreuth.ai4.petter.contentbasedcalculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ContentBasedCalculationResult {

    private String activityId;
    private double predictedRating;
    private int ratingCount;
    private double similarity;
    private String similarActivityId;

    public ContentBasedCalculationResult(String activityId) {
        this.activityId = activityId;
        this.predictedRating = -1;
        this.ratingCount = 0;
        this.similarity = -1;
        this.similarActivityId = "";
    }

    @Override
    public String toString() {
        return "ActivityId: " + activityId +
                "predictedRating: " + predictedRating +
                "ratingCount: " + ratingCount +
                "similarity: " + similarity +
                "similarActivityId: " + similarActivityId;
    }
}
