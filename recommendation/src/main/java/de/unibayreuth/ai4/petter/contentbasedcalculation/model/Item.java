package de.unibayreuth.ai4.petter.contentbasedcalculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Item {
    private String id;
    private Vector<ItemVector> vector;
}
