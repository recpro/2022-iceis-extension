package de.unibayreuth.ai4.petter.contentbasedcalculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ItemSet {
    private Item a;
    private Set<Item> items;
}
