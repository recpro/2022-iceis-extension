package de.unibayreuth.ai4.petter.contentbasedcalculation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ItemVector {
    private String id;
    private double value;
}
