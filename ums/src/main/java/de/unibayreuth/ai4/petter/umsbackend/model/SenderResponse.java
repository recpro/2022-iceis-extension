package de.unibayreuth.ai4.petter.umsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class SenderResponse {
    private String message;
}
