package de.unibayreuth.ai4.petter.umsbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@Setter
@Getter
public class User {

    private String id; // = KecyloakID!
    private String firstName;
    private String lastName;
    private Set<String> classifierIds;

    public User() {
        this.id = "-1";
        this.firstName = "";
        this.lastName = "";
        this.classifierIds = new HashSet<>();
    }
}
