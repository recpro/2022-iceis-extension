package de.unibayreuth.ai4.petter.umsbackend.user;

import de.unibayreuth.ai4.petter.umsbackend.model.SenderResponse;
import de.unibayreuth.ai4.petter.umsbackend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(path = "/test")
    public SenderResponse test() {
        return new SenderResponse("Test from UMS-backend");
    }

    @GetMapping(path = "/getAll")
    public ResponseEntity<Set<User>> getAll() throws IOException {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping(path = "/getById")
    public ResponseEntity<User> getById(@RequestParam(name = "id") String id) throws IOException {
        return ResponseEntity.ok(userService.getUserById(id));
    }
}
