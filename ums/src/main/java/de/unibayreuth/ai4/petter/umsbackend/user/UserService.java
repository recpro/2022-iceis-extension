package de.unibayreuth.ai4.petter.umsbackend.user;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.unibayreuth.ai4.petter.umsbackend.model.User;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

@Service
public class UserService {
    public User getUserById(String id) throws IOException {
        Set<User> users = this.readUsersFromJson();
        return users != null ?
                users.stream().filter(u -> Objects.equals(u.getId(), id)).toList().stream().findFirst().orElse(new User())
                : new User();
    }

    public Set<User> getAll() throws IOException {
        return this.readUsersFromJson();
    }

    private Set<User> readUsersFromJson() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new File("json/users.json"),new TypeReference<>(){});
    }
}
